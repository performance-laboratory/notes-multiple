
document.addEventListener("DOMContentLoaded",  function() {
	init();
}, false);

function init() {

    ScreenSize();

    var aside_tags = document.querySelectorAll("aside span");
    for (var i = aside_tags.length - 1; i >= 0; i--) {
        aside_tags[i].addEventListener("click", tri);
    }
    var note_headers = document.querySelectorAll(".note h1, .note h2, .note h3, .note h4");
    for (var i = 0; i < note_headers.length; i++) {
        note_headers[i].addEventListener("click", tri);
    }

    masonry();

    // sur écrans mobiles
    var menu_button = document.querySelectorAll(".menu_button");
    for (var i = 0; i < menu_button.length; i++) menu_button[i].addEventListener("click", toggleMenu);
}


function masonry() {
    var elem = document.querySelector('main');
    var imgs = document.querySelectorAll(".note img");
    var imgLoad = imagesLoaded(imgs);

    if (!imgs.length) {
        msnry = new Masonry( elem, {
            itemSelector: ".selected",
            horizontalOrder: true,
            columnWidth: '.note',
            gutter: 20
        });
    } else {
        imgLoad.on( 'progress', function( instance, image ) {
            msnry = new Masonry( elem, {
                itemSelector: ".selected",
                horizontalOrder: true,
                columnWidth: '.note',
                gutter: 20
            });
        });
    }


    var font = new FontFaceObserver('Playfair Display');

    font.load().then(function () {
        msnry.layout();
    }, function () {
        console.log('Font is not available');
    });
    
    
}

function tri() {

    var valeur_tri = this.getAttribute("data-sort");
    var tri_corresp = document.querySelectorAll('aside [data-sort="'+valeur_tri+'"]');
    for (var i = 0; i < tri_corresp.length; i++) {
        tri_corresp[i].classList.toggle("choisi");
        var container = closest(tri_corresp[i], function(el) { return el.tagName === 'DETAILS'; });
        container.open = true;
    }

    // collection des choix
    var span_deja_choisi = document.querySelectorAll("aside span.choisi");
    var tableau_choix = [];
    for (var i = 0; i < span_deja_choisi.length; i++) {
        tableau_choix.push(span_deja_choisi[i].getAttribute('data-sort'));
    }

    // reset
    var note_deja_choisi = document.querySelectorAll(".note .choisi");
    for (var i = 0; i < note_deja_choisi.length; i++) {
        note_deja_choisi[i].classList.remove("choisi");
    }
    var deja_selected = document.querySelectorAll(".note.selected");
    for (var i = 0; i < deja_selected.length; i++) {
        deja_selected[i].classList.remove("selected");
    }

    /*
    // collection des résultats
    var tableau_resultat = [];
    for (var i = 0; i <= tableau_choix.length; i++) {
        var results = document.querySelectorAll('.note [data-sort="'+tableau_choix[i]+'"]');
        for (var j = 0; j < results.length; j++) {
            results[j].classList.add("choisi");
            var article = closest(results[j], function(el) {
                if (el.classList != undefined) return el.classList.contains('note');
            });
            if (tableau_resultat.indexOf(article) < 0) {
                tableau_resultat.push(article);
            }
        }
    }
    */

    // collection des résultats
    var tableau_resultat = [];
    var notes = document.querySelectorAll(".note");
    var ou = false;
    for (var i = 0; i < notes.length; i++) {
        var headers = notes[i].querySelectorAll('[data-sort]');
        var string_sorts = [];
        for (var j = 0; j < headers.length; j++) {
            var data_sort = headers[j].getAttribute("data-sort");
            if (tableau_choix.indexOf(data_sort) > -1) {
                headers[j].classList.add("choisi");
            }
            string_sorts.push(data_sort);
        }

        if (ou) {
            var results = tableau_choix.some(function(element){
                 return string_sorts.indexOf(element) != -1;
            });
        } else {
            var results = tableau_choix.every(function(element){
                return string_sorts.indexOf(element) != -1;
            });
        }
        if (results) {
            tableau_resultat.push(notes[i]);
        }
    }

    // sélection des résultats
    for (var i = 0; i < tableau_resultat.length; i++) {
        tableau_resultat[i].classList.add("selected");
    }

    // update masonry
    var main = document.querySelector("main");
    var sorry = document.querySelector(".sorry");
    if (tableau_resultat.length) {
        main.classList.add("filtered");

        msnry.reloadItems();
        msnry.layout();

        sorry.classList.remove("visible");
    } else if (!tableau_resultat.length) {
        sorry.classList.add("visible");
        console.log("pas de résultats");
    } else {
        main.classList.remove("filtered");
        var all_notes = document.querySelectorAll(".note");
        for (var i = 0; i < all_notes.length; i++) {
            all_notes[i].classList.add("selected");
        }
        
        msnry.reloadItems();
        msnry.layout();

        sorry.classList.remove("visible");
    }
    var headers = ["H1", "H2", "H3", "H4"];
    if (!headers.includes(this.nodeName)) {
        toggleMenu();
    }    
}


var closest = function(el, fn) {
    return el && (fn(el) ? el : closest(el.parentNode, fn));
}





/* INTERACTION */

function toggleMenu() {
    console.log("menu");
    var aside = document.querySelector("aside");
    aside.classList.toggle("visible");
}

/* MODULES */

var closest = function(el, fn) {
    return el && (fn(el) ? el : closest(el.parentNode, fn));
}




















function ScreenSize(){
    var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0];

    var w2 = window,
    d2 = document,
    e2 = d2.documentElement,
    g2 = d2.getElementsByTagName('body')[0];
    
    screen_height = w2.innerHeight|| e2.clientHeight|| g2.clientHeight;
    screen_width = w2.innerWidth|| e2.clientWidth|| g2.clientWidth;
}

/*



************ AIDE MÉMOIRE JS ***********

****** AFFICHER UN RÉSULTAT DANS LA CONSOLE ******

console.log();




****** SÉLECTION D'ÉLÉMENT ******

var el = document.getElementById("mon_id");
var el = document.getElementsByClassName("ma_classe")[];
var el = document.getElementsByTagName("ma_balise")[];
var el = document.querySelector("mon_sélecteur");
var el = document.querySelectorAll("mon_sélecteur")[];




****** DÉCLARATION DE FONCTION ******

function maFonction() {
	console.log("hello");
}




****** BOUCLE FOR ******

for (i = 0; i < 10; i++) {
	console.log(i);
}




****** CONDITION ******

var condition = true;
if (condition) {
	console.log("vrai");
}

if (condition1) {
	console.log("condition1 : vrai");
} else if (condition2) {
	console.log("condition2 : vrai");
} else {
	console.log("condition1 et condition2 : faux");
}




****** ÉVÉNEMENT ******

element.addEventListener(mon_event, ma_fonction);
element.removeEventListener(mon_event, ma_fonction)

"click" "mousedown" "mousemove" "mouseover" "mouseenter" "mouseleave"
"keypress" "keydown" "keyup"




****** TOUCHES CLAVIER ******

À venir…




****** CRÉATION D'ÉLÉMENT ******

var el = document.createElement('div');
var el = document.createTextNode();

container.appendChild(el);




****** INSERTION D'ÉLÉMENT ******

container.appendChild(el);
container.removeChild(el);




****** ÉLÉMENT : ID ET ATTRIBUT ******

el.id = "mon_id";

el.setAttribute("nom_attribut",  "valeur_attribut");
el.getAttribute("nom_attribut");  




****** ÉLÉMENT : LISTE DE CLASS ******

element.classList.toggle();
el.classList.remove();
el.classList.add();
el.classList.contains();




****** NOMBRE D'ÉLÉMENTS / LONGUEUR D'UN ÉLÉMENT ******

var nb = el.length;




****** STYLE ******

el.style.propriété_css = "valeur";




****** TAILLE DE LA FENETRE ******

var screen_width = window.innerWidth;
var screen_height = window.innerHeight;




****** VIDÉO ******

video.load();
video.pause();
video.play();
video.setAttribute("onended",  "maFonction()");
video.setAttribute("controls",  "true");




****** GESTION DE TABLEAU ******

var tableau = [];
tableau.push();




****** GESTION DE CHAÎNE DE CARACTÈRES ******

el.toLowerCase().split(' ').join('-').replace();




****** ATTENTE ET RÉPÉTITION ******

setTimeout(function(){
	console.log("hello");
}, 3000);

setInterval(maFonction, 1000);




****** OPÉRATEURS ******

== égal à

!= différent de

=== contenu et type égal à

!== contenu ou type différent de

> supérieur à

>= supérieur ou égal à

< inférieur à

<= inférieur ou égal à




****** CONDITIONS ******

&& ET

|| OU

! NON



*/




