<?php

$dossierRoot = rtrim("../", '/\\');
$dossierVendor = $dossierRoot.'/vendor/';
require_once $dossierVendor.'autoload.php';

$tableau_noms = [];

foreach ($_FILES as $key => $file) {
	// print_r($file);
	$image = new Bulletproof\Image($file);
	$min = 1;
	$max = 2000000;
	$image->setSize($min, $max); 
	$image->setLocation('../uploads');

	$upload = $image->upload(); 
 
	if ($upload) {
		array_push($tableau_noms, $image->getName().".".$image->getMime());
	    // echo $upload->getLocation();
	} else {
	    echo $image["error"]; 
	}
}

echo json_encode($tableau_noms);
