<?php
global $dossierRoot;
global $titre;
global $categories;
global $pages;
global $pages2;
global $pages_collab;
global $dossierContenu;
?>

<!DOCTYPE html>
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="fr">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">

	<title><?= $titre ?></title>

	<link rel="stylesheet" href="<?=getBaseUrl()?>/admin/assets/node_modules/easymde/dist/easymde.min.css">
	<script src="<?=getBaseUrl()?>/admin/assets/node_modules/easymde/dist/easymde.min.js" defer></script>

	<link rel="stylesheet" href="<?=getBaseUrl()?>/admin/assets/style.css" type="text/css"/>
	<link rel="stylesheet" href="<?=getBaseUrl()?>/admin/assets/theme.css" type="text/css"/>

	<script type="text/javascript" src="<?=getBaseUrl()?>/admin/assets/node_modules/sortablejs/Sortable.min.js" defer></script>
	<script type="text/javascript" src="<?=getBaseUrl()?>/admin/assets/javascript.js" defer></script>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

</head>
<body>



<main>
	<button class='menu_button fas fa-bars'></button>
</main>

<aside>
	<div class="sticky">

		<button class='menu_button fas fa-bars'></button>
		
		<div id="menu">
			<a href="<?=getBaseUrl()?>">Parcourir</a>
			<a href="<?=getBaseUrl()?>ecrire/">Ecrire</a>	
		</div>


		<div id="liste_pages">

			<button class='plus' type='button'>New note</button>
			<button class='plus_pad' type='button'>New pad</button>


<?php foreach ($pages2 as $key => $page): ?>

<?php
$file = file_get_contents("contenu/".$page->key."/meta.json");
$data = json_decode($file);
$titre = $data->{'titre'};

if (!strlen($titre)) {
	$titre = "<em>Untitled</em>";
}
?>
<?php if ($data->{'type'} === "local"): ?>
			<li data-key='<?= $page->key ?>'>
				<span><?=$titre?></span><div class="fas fa-pen"></div>
				<div class='fas fa-grip-vertical handler'></div>
			</li>
<?php else: ?>
			<li data-key='<?= $page->key ?>' data-url='<?= $page->path ?>'>
				<span class="pad"><?=$titre?></span><div class="fas fa-pen"></div>
				<div class='fas fa-grip-vertical handler'></div>
			</li>
<?php endif; ?>

<?php endforeach ?>
		</div>

		<div class="upload" id="img_list" data-url="<?=getBaseUrl()?>">
			<form class="upload_form" method="post" enctype="multipart/form-data" autocomplete='off'>
				<input type="hidden" name="MAX_FILE_SIZE" value="1000000"/>
			    <input type="file" class="selectedFile" multiple style="display: none;" />
				<button type="button" onclick="document.querySelector('#img_list .selectedFile').click();">New image</button>
			</form>


<?php foreach(files_in_dir($dossierRoot.'uploads') as $image): ?>
			<div class="ligne">
				<figure>
					<img src="../uploads/<?= $image ?>">
				</figure>
				<p><?=getBaseUrl()?>uploads/<?= $image ?></p>
			</div>
<?php endforeach ?>
		</div>

		<div class="upload" id="snd_list" data-url="<?=getBaseUrl()?>">
			<form class="upload_form" method="POST" enctype="multipart/form-data" autocomplete='off'>
				<input type="hidden" name="MAX_FILE_SIZE" value="1000000"/>
			    <input type="file" class="selectedFile" multiple style="display: none;" />
				<button type="button" onclick="document.querySelector('#snd_list .selectedFile').click();">New sound</button>
			</form>

<?php foreach(files_in_dir($dossierRoot.'uploads_audio') as $son): ?>
			<div class="ligne">
				<audio controls src="<?=getBaseUrl()?>uploads_audio/<?= $son ?>"></audio>
				<p><?=getBaseUrl()?>uploads_audio/<?= $son ?></p>
			</div>
<?php endforeach ?>

		</div>


		<div class="upload" id="vid_list" data-url="<?=getBaseUrl()?>">
			<form class="upload_form" method="POST" enctype="multipart/form-data" autocomplete='off'>
				<input type="hidden" name="MAX_FILE_SIZE" value="1000000"/>
			    <input type="file" class="selectedFile" multiple style="display: none;" />
				<button type="button" onclick="document.querySelector('#vid_list .selectedFile').click();">New video</button>
			</form>

<?php foreach(files_in_dir($dossierRoot.'uploads_video') as $vid): ?>
			<div class="ligne">
				<video controls src="<?=getBaseUrl()?>uploads_video/<?= $vid ?>"></video>
				<p><?=getBaseUrl()?>uploads_video/<?= $vid ?></p>
			</div>
<?php endforeach ?>

		</div>
	</div>

</aside>



<template class="note_template">
	<div class='note' data-categories="<?= $categories[0] ?> <?= $categories[1] ?> <?= $categories[2] ?> <?= $categories[3] ?>">

		<form>
			<label for="name">Title</label>
			<input type="text" name="titre" class="titre" autocomplete='off'>
			<label for="name">Author</label>
			<input type="text" name="auteur" class="auteur" autocomplete='off'>
		</form>
		
		<div class='editor'>
			<textarea autocomplete='off'></textarea>
		</div>

		<button class='save fas fa-save'></button>
		<button class='suppr fas fa-trash-alt'></button>
	
	</div>
	
</template>



<template class="pad_template">
	<div class='note pad'>
		<form>
			<label for="name">Titre</label>
			<input type="text" name="titre" class="titre" autocomplete='off'>
			<label for="name">Auteur</label>
			<input type="text" name="auteur" class="auteur" autocomplete='off'>
			<label for="name">Pad url</label>
			<input type="text" name="url" class="url" autocomplete='off'>
		</form>

		<iframe src=""></iframe>

		<button class='save fas fa-save'></button>
		<button class='suppr fas fa-trash-alt'></button>
		
	</div>
</template>



<template class="li_template">
	<li>
		<span></span><div class="fas fa-pen"></div>
		<div class='fas fa-grip-vertical handler'></div>
	</li>
</template>



<template class="img_template">
	<div class="ligne">
		<figure>
			<img src="">
		</figure>
		<p></p>
	</div>
</template>


<template class="snd_template">
	<div class="ligne">
		<audio controls src=""></audio>
		<p></p>
	</div>
</template>


<template class="vid_template">
	<div class="ligne">
		<video controls src=""></video>
		<p></p>
	</div>
</template>



</body>
</html>
